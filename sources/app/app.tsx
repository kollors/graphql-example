import { routes } from 'app/routes';
import { FC, Suspense } from 'react';
import { Route, Switch, NavLink } from 'react-router-dom';

const menuItems = Object.fromEntries(
  Object
    .entries(routes)
    .filter(([, value]) => !/:\w+/.test(value.path))
);

export const App: FC = () => {
  return (
    <div className="container my-3">
      <div className="border border-secondary bg-white p-2 rounded">
        <ul className="nav nav-pills">
          {Object.entries(menuItems).map(([key, value]) => (
            <li key={key} className="nav-item">
              <NavLink className="nav-link" to={value.path}>
                {value.title}
              </NavLink>
            </li>
          ))}
        </ul>
      </div>

      <div className="container mt-3">
        <Suspense fallback={<b>Loading...</b>}>
          <Switch>
            {Object.entries(routes).map(([key, value]) => (<Route key={key} {...value}/>))}
          </Switch>
        </Suspense>
      </div>
    </div>
  );
}

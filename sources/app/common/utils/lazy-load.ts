import { ComponentType, lazy, LazyExoticComponent } from 'react';

export function lazyLoad<C extends ComponentType>(factory: () => Promise<C>): LazyExoticComponent<C> {
  return lazy(() => factory().then((module) => ({ default: module })));
}

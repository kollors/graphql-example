export function routeUrl<T extends Record<string, number | string>>(path: string, params: T): string {
  return path
    .split('/')
    .map((part) => part.startsWith(':') ? params[part.substr(1)] : part)
    .join('/');
}

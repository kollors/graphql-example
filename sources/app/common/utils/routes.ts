import { RouteProps } from 'react-router-dom';

export type RoutesOptions = Record<string, RouteProps & { group?: string | string[], priority?: number, title: string }>;
export type Routes<T extends RoutesOptions = RoutesOptions> = T;

export function createRoutes<T extends RoutesOptions>(options: T): Routes<T> {
  return options;
}

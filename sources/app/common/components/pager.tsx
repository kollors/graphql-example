import { FC, useState } from 'react';

export interface PagerProps {
  skip: number;
  take: number;
  onChange: (event: { skip: number, take: number }) => void;
}

export const Pager: FC<PagerProps> = (props) => {
  const [form, setForm] = useState({ skip: props.skip, take: props.take });

  return (
    <div className="g-3 row mt-2">
      <div className="col-12 col-md-6">
        <div className="form-floating">
          <input
            className="form-control"
            type="number"
            value={form.skip}
            onChange={(e) => setForm({ ...form, skip: +e.target.value })}
          />

          <label>Skip</label>
        </div>
      </div>

      <div className="col-12 col-md-6">
        <div className="form-floating">
          <input
            className="form-control"
            type="number"
            value={form.take}
            onChange={(e) => setForm({ ...form, take: +e.target.value })}
          />

          <label>Take</label>
        </div>
      </div>

      <div className="col-12">
        <button className="btn btn-primary w-100" onClick={() => props.onChange(form)}>Apply</button>
      </div>
    </div>
  );
};

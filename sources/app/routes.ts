import { createRoutes, lazyLoad } from 'app/common/utils';

export const routes = createRoutes({
  movie: {
    component: lazyLoad(() => import('app/pages/movie-page').then((m) => m.MoviePage)),
    group: ['mainRoutes'],
    path: '/movies/:id',
    title: 'Movie',
  },
  movies: {
    component: lazyLoad(() => import('app/pages/movies-page').then((m) => m.MoviesPage)),
    path: '/movies',
    title: 'Movies',
  },
  director: {
    component: lazyLoad(() => import('app/pages/director-page').then((m) => m.DirectorPage)),
    path: '/users/:id/info',
    title: 'Director',
  },
  directors: {
    component: lazyLoad(() => import('app/pages/directors-page').then((m) => m.DirectorsPage)),
    path: '/directors',
    title: 'Directors',
  },
});

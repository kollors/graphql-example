import { DirectorFormQuery, useCreateDirectorFormMutation, useDirectorFormLazyQuery, useUpdateDirectorFormMutation } from 'app/graphql';
import { FC, SyntheticEvent, useEffect, useState } from 'react';

export interface DirectorFormProps {
  id: string | number;
}

export const DirectorForm: FC<DirectorFormProps> = (props) => {
  const [form, setForm] = useState<DirectorFormQuery>();
  const [execute, query] = useDirectorFormLazyQuery();
  const [create, createMutation] = useCreateDirectorFormMutation();
  const [update, updateMutation] = useUpdateDirectorFormMutation();
  const loading = createMutation.loading || updateMutation.loading;

  const handleFormSubmit = async (event: SyntheticEvent) => {
    event.preventDefault();

    if (+props.id !== 0) {
      await update({ variables: form?.director });
      await updateMutation.client.cache.reset();
    } else {
      await create({ variables: form?.director });
      await createMutation.client.cache.reset();
    }
  };

  useEffect(() => {
    if (+props.id !== 0) {
      execute({ variables: { id: `${props.id}` } });
    } else {
      query.data = {};
    }
  }, [props.id]);

  useEffect(() => setForm(query.data), [query.data]);

  if (query.loading || form == null) {
    return (<b>Loading director form...</b>)
  }

  return (
    <form onSubmit={handleFormSubmit}>
      <div className="form-floating mb-3">
        <input
          className="form-control"
          disabled={loading}
          required
          value={form.director?.fullName}
          onChange={(e) => setForm({ ...form, director: { ...form.director, fullName: e.target.value }  })}
        />

        <label>FullName</label>
      </div>

      <div className="btn-group w-100">
        <button className="btn btn-primary w-50" disabled={loading}>
          Save
        </button>
      </div>
    </form>
  );
};

import { MovieFormQuery, useCreateMovieFormMutation, useMovieFormLazyQuery, useUpdateMovieFormMutation } from 'app/graphql';
import { FC, SyntheticEvent, useEffect, useState } from 'react';

export interface MovieFormProps {
  id: string | number;
}

export const MovieForm: FC<MovieFormProps> = (props) => {
  const [form, setForm] = useState<MovieFormQuery>();
  const [execute, query] = useMovieFormLazyQuery();
  const [create, createMutation] = useCreateMovieFormMutation();
  const [update, updateMutation] = useUpdateMovieFormMutation();
  const loading = createMutation.loading || updateMutation.loading;

  const handleFormSubmit = async (event: SyntheticEvent) => {
    event.preventDefault();

    if (+props.id !== 0) {
      await update({ variables: form?.movie });
      await updateMutation.client.cache.reset();
    } else {
      await create({ variables: form?.movie });
      await createMutation.client.cache.reset();
    }
  };

  useEffect(() => {
    execute({ variables: { id: `${props.id}` } });
  }, [props.id]);

  useEffect(() => setForm(query.data), [query.data]);

  if (query.loading || form == null) {
    return (<b>Loading movie form...</b>)
  }

  return (
    <form onSubmit={handleFormSubmit}>
      <div className="form-floating mb-3">
        <input
          className="form-control"
          disabled={loading}
          required
          value={form.movie?.title}
          onChange={(e) => setForm({ ...form, movie: { ...form.movie, title: e.target.value }  })}
        />

        <label>Title</label>
      </div>

      <div className="form-floating mb-3">
        <select
          className="form-select"
          required
          value={form.movie?.directorId}
          onChange={(e) => setForm({ ...form, movie: { ...form.movie, directorId: e.target.value }  })}
        >
          {form.directors?.map((director) => (
            <option key={director.id} value={director.id}>{director.fullName}</option>
          ))}
        </select>

        <label>Director</label>
      </div>

      <div className="btn-group w-100">
        <button className="btn btn-primary w-50" disabled={loading}>
          Save
        </button>
      </div>
    </form>
  );
};

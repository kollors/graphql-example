import { ApolloClient, InMemoryCache, from } from '@apollo/client';
import { BatchHttpLink } from '@apollo/client/link/batch-http';

const batchHttpLink = new BatchHttpLink({
  batchInterval: 0,
  uri: '/graphql',
});

const inMemoryCache = new InMemoryCache({
  addTypename: false,
});

export const apolloClient = new ApolloClient({
  cache: inMemoryCache,
  connectToDevTools: process.env.NODE_ENV !== 'production',
  link: from([batchHttpLink]),
  uri: '/graphql',
});

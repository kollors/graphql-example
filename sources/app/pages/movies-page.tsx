import { Pager } from 'app/common/components';
import { routeUrl } from 'app/common/utils';
import { MovieForm } from 'app/feature/movie';
import { useMoviesPageQuery } from 'app/graphql';
import { routes } from 'app/routes';
import { parse, stringify } from 'query-string';
import { FC, useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom'

export const MoviesPage: FC = () => {
  const history = useHistory();
  const params = Object.assign({ take: 10, skip: 0 }, parse(history.location.search, { parseNumbers: true }));
  const query = useMoviesPageQuery({ variables: { take: params.take, skip: params.skip } });
  const [selectedId, setSelectedId] = useState<number | string>();

  if (query.loading || query.data == null) {
    return (<b>Loading movies page...</b>);
  }

  return (
    <div>
      {selectedId != null && (
        <div className="border border-secondary rounded p-2 mb-2">
          <MovieForm id={selectedId}/>

          <button
            className="btn btn-secondary mt-2 w-100"
            onClick={() => setSelectedId(undefined)}
          >
            Cancel
          </button>
        </div>
      )}

      {selectedId == null && (
        <>
          <ul className="list-group">
            <li className="align-items-center d-flex justify-content-end list-group-item">
              <button className="btn btn-sm btn-secondary" onClick={() => setSelectedId(0)}>
                Create
              </button>
            </li>

            {query.data.movies?.map((movie) => (
              <li key={movie.id} className="align-items-center d-flex justify-content-between list-group-item">
                <div>
                  <NavLink to={routeUrl(routes.movie.path, { id: movie.id as string })}>
                    {movie.title}
                  </NavLink>

                  <div className="text-muted">{movie.director?.fullName}</div>
                </div>

                <button className="btn btn-sm btn-secondary" onClick={() => setSelectedId(movie.id as string)}>
                  Update
                </button>
              </li>
            ))}
          </ul>

          <Pager
            skip={params.skip}
            take={params.take}
            onChange={(event) => history.push({ search: stringify({ ...params, ...event }) })}
          />
        </>
      )}
    </div>
  );
};

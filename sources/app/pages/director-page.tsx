import { routeUrl } from 'app/common/utils';
import { useDirectorPageQuery } from 'app/graphql';
import { routes } from 'app/routes';
import { FC } from 'react';
import { NavLink, useParams } from 'react-router-dom';

export const DirectorPage: FC = () => {
  const params = Object.assign({ id: '0' }, useParams<{ id: string }>());
  const query = useDirectorPageQuery({ variables: { id: params.id } });

  if (query.loading || query.data == null) {
    return (<b>Loading director page...</b>);
  }

  return (
    <div>
      <NavLink to={routeUrl(routes.directors.path, {})}>
        Back
      </NavLink>

      <pre>{JSON.stringify(query.data.director, null, 2)}</pre>
    </div>
  );
};

import { Pager } from 'app/common/components';
import { routeUrl } from 'app/common/utils';
import { DirectorForm } from 'app/feature/director';
import { useDirectorsPageQuery } from 'app/graphql';
import { routes } from 'app/routes';
import { parse, stringify } from 'query-string';
import { FC, useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom'

export const DirectorsPage: FC = () => {
  const history = useHistory();
  const params = Object.assign({ take: 10, skip: 0 }, parse(history.location.search, { parseNumbers: true }));
  const query = useDirectorsPageQuery({ variables: { take: params.take, skip: params.skip } });
  const [selectedId, setSelectedId] = useState<number | string>();

  if (query.loading || query.data == null) {
    return (<b>Loading directors page...</b>);
  }

  return (
    <div>
      {selectedId != null && (
        <div className="border border-secondary rounded p-2 mb-2">
          <DirectorForm id={selectedId}/>

          <button
            className="btn btn-secondary mt-2 w-100"
            onClick={() => setSelectedId(undefined)}
          >
            Cancel
          </button>
        </div>
      )}

      {selectedId == null && (
        <>
          <ul className="list-group">
            <li className="align-items-center d-flex justify-content-end list-group-item">
              <button className="btn btn-sm btn-secondary" onClick={() => setSelectedId(0)}>
                Create
              </button>
            </li>

            {query.data.directors?.map((director) => (
              <li key={director.id} className="align-items-center d-flex justify-content-between list-group-item">
                <NavLink to={routeUrl(routes.director.path, { id: director.id as string })}>
                  {director.fullName}
                </NavLink>

                <button className="btn btn-sm btn-secondary" onClick={() => setSelectedId(director.id as string)}>
                  Update
                </button>
              </li>
            ))}
          </ul>

          <Pager
            skip={params.skip}
            take={params.take}
            onChange={(event) => history.push({ search: stringify({ ...params, ...event }) })}
          />
        </>
      )}
    </div>
  );
};

import { routeUrl } from 'app/common/utils';
import { useMoviePageQuery } from 'app/graphql';
import { routes } from 'app/routes';
import { FC } from 'react';
import { NavLink, useParams } from 'react-router-dom';

export const MoviePage: FC = () => {
  const params = Object.assign({ id: '0' }, useParams<{ id: string }>());
  const query = useMoviePageQuery({ variables: { id: params.id } });

  if (query.loading || query.data == null) {
    return (<b>Loading movie page...</b>);
  }

  return (
    <div>
      <NavLink to={routeUrl(routes.movies.path, {})}>
        Back
      </NavLink>

      <pre>{JSON.stringify(query.data.movie, null, 2)}</pre>
    </div>
  );
};

import { config as envConfig } from 'dotenv';
import { GraphQLID, GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLSchema, GraphQLString } from 'graphql';
import { ApolloServer } from 'apollo-server';

envConfig();

const directors = [
  { id: 1, fullName: 'Tim Burton' },
  { id: 2, fullName: 'Steven Spielberg' },
];

const movies = [
  { id: 1, title: 'The Nightmare Before Christmas', directorId: 1 },
  { id: 2, title: 'Corpse Bride', directorId: 1 },
  { id: 3, title: 'Back to the Future', directorId: 2 },
  { id: 4, title: 'The Terminal', directorId: 2 },
];

const DirectorType = new GraphQLObjectType({
  name: 'Director',
  fields: () => ({
    id: { type: GraphQLID },
    fullName: { type: GraphQLString },
    movies: {
      type: new GraphQLList(MovieType),
      resolve: (parent) => {
        return movies.filter((movie) => `${movie.directorId}` === `${parent.id}`);
      },
    },
  }),
});

const MovieType = new GraphQLObjectType({
  name: 'Movie',
  fields: () => ({
    id: { type: GraphQLID },
    title: { type: GraphQLString },
    directorId: { type: GraphQLID },
    director: {
      type: DirectorType,
      resolve: (parent) => {
        return directors.find((director) => `${director.id}` === `${parent.directorId}`);
      },
    },
  }),
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    createDirector: {
      type: GraphQLInt,
      args: { fullName: { type: GraphQLString } },
      resolve: (parent, args: { fullName: string }) => {
        const directorId = directors.length + 1;
        const director = { id: directorId, fullName: args.fullName };

        directors.push(director);

        return directorId;
      },
    },
    updateDirector: {
      type: GraphQLInt,
      args: { id: { type: GraphQLID }, fullName: { type: GraphQLString } },
      resolve: (parent, args: { id: number | string, fullName: string }) => {
        const index = directors.findIndex((director) => `${director.id}` === `${args.id}`);

        directors[index] = Object.assign(directors[index], args);

        return directors[index].id;
      },
    },
    createMovie: {
      type: GraphQLInt,
      args: { title: { type: GraphQLString }, directorId: { type: GraphQLID } },
      resolve: (parent, args: { title: string, directorId: number | string }) => {
        const movieId = movies.length + 1;
        const movie = { id: movieId, title: args.title, directorId: +args.directorId };

        movies.push(movie);

        return movieId;
      },
    },
    updateMovie: {
      type: GraphQLInt,
      args: { id: { type: GraphQLID }, title: { type: GraphQLString }, directorId: { type: GraphQLID } },
      resolve: (parent, args: { id: number | string, title: string, directorId: number | string }) => {
        const index = movies.findIndex((movie) => `${movie.id}` === `${args.id}`);

        movies[index] = Object.assign(movies[index], args);

        return movies[index].id;
      },
    },
  }),
});

const Query = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    director: {
      type: DirectorType,
      args: { id: { type: GraphQLID } },
      resolve: (parent, args: { id: number | string }) => {
        return directors.find((director) => `${director.id}` === `${args.id}`);
      },
    },
    directors: {
      type: new GraphQLList(DirectorType),
      args: { take: { type: GraphQLInt }, skip: { type: GraphQLInt } },
      resolve: (parent, args: { take: number, skip: number }) => {
        const { skip = 0, take = directors.length } = args;

        return directors.slice(skip, skip + take);
      },
    },
    movie: {
      type: MovieType,
      args: { id: { type: GraphQLID } },
      resolve: (parent, args: { id: number | string }) => {
        return movies.find((movie) => `${movie.id}` === `${args.id}`);
      },
    },
    movies: {
      type: new GraphQLList(MovieType),
      args: { take: { type: GraphQLInt }, skip: { type: GraphQLInt } },
      resolve: (parent, args: { take: number, skip: number }) => {
        const { skip = 0, take = movies.length } = args;

        return movies.slice(skip, skip + take);
      },
    },
  }),
});

const schema = new GraphQLSchema({
  mutation: Mutation,
  query: Query,
});

const server = new ApolloServer({
  schema,
});

server
  .listen(process.env.SERVER_PORT)
  .then(({ url }) => console.log(url))
  .catch(console.error);

import { ApolloProvider } from '@apollo/client';
import { App } from 'app/app';
import { apolloClient } from 'app/apollo';
import { createElement } from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './index.scss';

document.addEventListener('DOMContentLoaded', () => {
  const app = createElement(App);
  const browserRouter = createElement(BrowserRouter, {}, app);
  const apolloProvider = createElement(ApolloProvider, { client: apolloClient, children: browserRouter });

  render(apolloProvider, document.getElementById('root-app'));
});

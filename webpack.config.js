const { spawn } = require('child_process');
const { config: envConfig } = require('dotenv');
const HtmlPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { join } = require('path');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const packageConfig = require('./package.json');

envConfig();

const prodMode = process.env.NODE_ENV === 'production';

const paths = {
  context: join(__dirname, 'sources'),
  output: join(__dirname, 'bundle'),
};

const webpackConfig = {
  context: paths.context,
  entry: '.',
  experiments: {
    asset: true,
  },
  devServer: {
    contentBase: paths.output,
    historyApiFallback: true,
    host: process.env.CLIENT_HOST,
    port: process.env.CLIENT_PORT,
    proxy: {
      '/graphql': {
        changeOrigin: true,
        target: process.env.CLIENT_PROXY,
      },
    },
  },
  devtool: prodMode ? false : 'source-map',
  mode: process.env.NODE_ENV,
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(woff2?|eot|ttf|otf|svg)$/i,
        type: 'asset/inline',
      },
      {
        test: /\.(ico|gif|png|jpg|jpeg)$/i,
        type: 'asset/resource',
      },
    ],
  },
  optimization: {
    chunkIds: prodMode ? 'natural' : 'named',
    minimizer: [],
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        default: false,
        defaultVendors: {
          name: 'vendors',
          priority: -1,
          test: /[\\/]node_modules[\\/]/,
        },
        react: {
          name: 'react',
          test: /[\\/]node_modules[\\/]react/,
        },
      },
    },
  },
  output: {
    assetModuleFilename: join('assets', prodMode ? '[id][hash][ext]' : '[name][ext]'),
    chunkFilename: join('scripts', prodMode ? '[id][hash].js' : '[name].js'),
    filename: join('scripts', prodMode ? '[id][hash].js' : '[name].js'),
    path: paths.output,
    publicPath: '/',
  },
  performance: {
    hints: false,
  },
  plugins: [
    {
      apply(compiler) { // очищаем папку про production сборке
        spawn('rm', ['-rf', compiler.options.output.path]);
      },
    },
    new DefinePlugin({
      'process.env': JSON.stringify(process.env),
    }),
    new HtmlPlugin({
      filename: 'index.html',
      template: join(paths.context, 'index.html'),
      title: packageConfig.name,
    }),
    new MiniCssExtractPlugin({
      filename: join('styles', prodMode ? '[id][hash].css' : '[name].css'),
    })
  ],
  resolve: {
    alias: {
      app: join(paths.context, 'app'),
    },
    extensions: [
      '.tsx',
      '.ts',
      '.js',
    ],
  },
};

module.exports = webpackConfig;

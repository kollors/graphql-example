**Установка зависимостей**

```
npm i
```

**Запуск сервера**

```
npm run start:server
```

**Генерация схемы**

```
npm run graphql:schema
```

**Генерация типов**

```
npm run graphql:codegen
```

**Запуск клиента**

```
npm run start:client
```
